# J'aimerais tester si un nombre est entier

s = 1.0
print(type(s) if s is type(int) else "Pas un entier")
print(type(s) == int)
x = 100
print(isinstance(x, int))
