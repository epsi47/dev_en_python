"""
Python et IA ou machine learning

Librairies IA :
AIMA ?
PyDatalog
Simple IA
GraphLab
PyBrain,
etc.
NumPy

Fonctionnalités de programmation "logique" et prédicats

Haskell, Lisp ou Erlang ?

Deep Blue ? --> prwtos upologisths pou nikhse ton pagkosmio prwta8lhth sto skaki
Jeu de Go (AlphaGo en 2016)
Test de Turing :
    Algos : ELIZA (implémenté en python)
            ALICE Bot

Méthodes pour faire réfléchir un ordinateur :
    Logique floue
    Algorithme génétique
    Data mining
    Réseaux de neuronnes
    Apprentissage automatique
"""

# SYNTAXE ET DECOUVERTE DU LANGAGE
# DATA TYPES
"""
str (texte)
int, float, complex (numérique)
list, tuple, range (Séquences)
dict (Mapping)
set, frozenset (Set)
Bool (Booléen)
bytes, bytearray, memoryview (binary)
NoneType (Type none)
"""

# Pour obtenir le type d'une variable
x = 5
print(type(x))

# Pour spécifier le type :
x = int(20)
print(type(x))

x = ["pomme", "banana", "cerise"]
print(x)
print(type(x))

x = tuple(("pomme", "banana", "cerise"))

# Complex type
x = 3 + 5j
print(type(x))

# Conversions :
x = float(1)
y = int(2.8)
z = complex(1)

print(x, y, z)

a = float(x)
b = int(y)
c = complex(x)

print(a, b, c, type(a), type(b), type(c))

# type string

a = """blabla fdgfgf aqdpo"""
print(a)

############################

a = "Salut"
print(a[1])

############################

for x in "Banane":
    print(x)

############################
"""J'ai une phrase, je souhaite 'détecter' un mot dans cette phrase"""

a = "Voila la phrase"
print("la" in a)

"""J'aimerais afficher quelque chose de plus sympa qu'un True ou False"""
print("Vrai" if "la" in a else "Faux")
# Solution :
a = "Voila la phrase"
if "la" in a:
    print("Trouvé")
if "la" not in a:
    print("Pas Trouvé")

# le slicing
s = "Exemple"
print(s[2:4])  # Le cinquième caractère donc celui dont l'index est 4 est exclu

# Comment afficher la position d'un caractère?
# Afficher que le m de exemple
print(s[3])
print(s.index("m"))

"""Si je veux afficher pl, au moins 2 solutions... J'aimerais partir à partir de l'envers de la chaîne"""
print(s[-3:-1])

# Tester
print(bool("Hello"))
print(bool(15))
print(bool(None))
print(bool(False))
print(bool(True))
print(bool(0))
print(bool(""))
print(bool(()))
print(bool([]))
print(bool({}))


#####################
def ma_fonction():
    return True


print(ma_fonction())

##################################

x = float(1)
y = float(3.4)
z = float("6")
s = float("4.5")

print(f"x : {type(x)}, y : {type(y)}, z : {type(z)}, s : {type(s)}")

"""Nomenclature et présentation d'un code Python
tuple python
"""
a = (3, 4, 7)
print(type(a))


# b, c = 5, 6 -> équivaut à (b, c) = (5, 6)

def test():
    return 5, 6


a = test()
print(a, type(a))
b, c = test()
print(b)
print("##################################################")
# rappel notre tuple était 5, 6
for i in a:
    print(i)

print(a[0])
print(a[1])

a = (3)

print(a, type(a))

b = (3,)
print(b, type(b))

# Récupérer l'élément unique présent dans le tuple

# approche 1
c = b[0]
print(c)

# approche 2
d, = b
print(d)

# autre syntaxe nom_variable, =
u = [5]
v, = u
print(v)

# Structure du code
"""
Titre
Nom du projet
Date de la dernière revision
Auteur
IDE
Client

##################
programme principal
##################

Zone des imports
Zone déclaration des variables globales
Zone déclaration modules et fonctions
PROGRAMME
"""

