"""Exercice 1
Déclarer différents types de variables.
La variable 'prénom' qui doit contenir la chaîne de caractère Pierre.
La variable 'age' qui doit contenir le nombre 20. La variable 'majeur' qui doit contenir un boolean vrai.
La variable 'compte_en_banque' qui doit contenir le nombre décimal 20135,384.
La variable 'amis' qui doit contenir une liste contenant trois chaînes de caractères : Marie, Julien et Adrien.
La variable 'parents' qui doit contenir un tuple contenant deux chaînes de caractères : Marc et Caroline.
"""

prenom = 'Pierre'
age = 20
majeur = True
compte_en_banque = 20135.384
amis = ['Marie', 'Julien', 'Adrien']
parents = ('Marc', 'Caroline')

print(prenom, age, majeur, compte_en_banque, amis, parents)

"""
Corriger l'erreur: 
site_web = "google''
print(site_web)
"""
site_web = "google"
print(site_web)

"""Exercice 3
Variable d'un type vers un autre
Après avoir déclaré une variable, afficher "Le nombre est 17"
"""
n = 17
print(f"Le nombre est {n}")
print("Le nombre est", n)
print("Le nombre est " + str(n))

"""Exercice 4
Trouver la valeur de la variable
On veut 'printer' la valeur que contient la variable a
a = 3
b = 6
a = b
b = 7
"""
print(6)

"""Exercice 5
a = 2
b = 6
c = 3
Comment printer les valeurs? et leur somme d'un coup? On veut afficher "2 + 6 + 3"
Contrainte: utiliser une nouvelle fonctionnalité python 3
"""
a = 2
b = 6
c = 3
print(a, b, c, sum([a, b, c]))
print(a, b, c, sep=" + ")

"""Exercice 6
list = range(3)
list2 = range(5)
print(list(list2))
"""
list1 = range(3)
list2 = range(5)
print(list(list2))

"""Exercice 7
Vérifier qu'une variable est bien d'un certain type
prenom = "Vincent"
#condition
prenom = 0
#condition
"""

prenom = "Vincent"
print("La variable est une chaîne" if type(prenom) == str else "La variable n'est pas une chaîne")
prenom = 0
print("La variable est un entier" if type(prenom) == int else "La variable n'est pas un entier")

"""Exercice 8
Remplacer un mot par un autre dans la chaîne
"Salut les dev". Remplacer Salut par Bonsoir
"""
s = "Salut les dev"
print(s)
s = s.replace("Salut", "Bonsoir")
print(s)

phrase = "salut les dev, salut alors ça va"
phrase = phrase.replace("salut", "Bonsoir", 1)